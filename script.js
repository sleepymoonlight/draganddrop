(function () {
    const nameItems = document.getElementsByClassName("draggable");
    const targets = document.getElementsByClassName("box");

    let isMouseDown = false;
    let currentItem = null;
    let draggableItem = null;
    let boxTarget = null;
    let mouseOffset = {
        x: 0,
        y: 0,
    };

    function onMouseDown(event, item) {
        event.preventDefault();
        isMouseDown = true;
        draggableItem = item.cloneNode(true);
        currentItem = item;
        mouseOffset = {
            x: item.offsetLeft - event.clientX,
            y: item.offsetTop - event.clientY,
        };

        draggableItem.classList.add("activeItem");
        currentItem.classList.add("activeShadow");

        document.body.appendChild(draggableItem);
    }

    function onMouseMove(event) {
        if (isMouseDown) {
            draggableItem.style.left = event.clientX + mouseOffset.x + "px";
            draggableItem.style.top = event.clientY + mouseOffset.y + "px";
        }
    }

    document.addEventListener("mousemove", onMouseMove);

    document.addEventListener("mouseup", () => {
        onDocumentMouseUp();
    });

    function onMouseUp() {
        if (!draggableItem) {
            return;
        }
        if (boxTarget) {
            let temp = currentItem.parentNode.removeChild(currentItem);
            boxTarget.appendChild(temp);
        }

        boxTarget.classList.remove("activeShadow");
        currentItem.classList.remove("activeShadow");
        draggableItem.style.pointerEvents = "auto";

        clear();
    }

    function onDocumentMouseUp(){
        if(!draggableItem || boxTarget){
            return;
        }

        currentItem.classList.remove("activeShadow");
        clear();
    }

    function onTargetMouseOver(event, target) {
        if (isMouseDown && draggableItem) {
            boxTarget = target;

            boxTarget.classList.add("activeShadow");
        }
    }

    function clear() {
        document.removeEventListener("mousemove", onMouseMove);
        isMouseDown = false;
        boxTarget = null;
        draggableItem.parentNode.removeChild(draggableItem);
        draggableItem = null;
        currentItem = null;
    }

    for (let i = 0; i < nameItems.length; i++) {
        let currentItem = nameItems[i];

        currentItem.addEventListener("mousedown", (event) => {
            onMouseDown(event, currentItem);
        });
    }

    for (let i = 0; i < targets.length; i++) {
        let target = targets[i];

        target.addEventListener("mouseenter", () => {
            onTargetMouseOver(event, target);
        });

        target.addEventListener("mouseup", (event) => {
            onMouseUp(event, draggableItem);
        });
    }
})();